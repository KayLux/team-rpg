import pygame
import time
import random

pygame.init()

#Color Defines
white = (255,255,255)
black = (0,0,0)
red = (255,69,0)
light_red = (255,0,0)
green = (32,178,170)
light_green = (102,205,170)
blue = (65,105,255)
light_blue = (135,206,250)

#PowerUp Defines
speed = 1
jump = 2

#Game Variables
display_width = 1000
display_height = 600
fps = 30
cameraX = 0 #cameraPos (only scrolls in x axis)

#Game/Display Setup
gameDisplay = pygame.display.set_mode((display_width, display_height)) #Creating Display surface
pygame.display.set_caption('Team Peter The Game')# Window heading
pygame.display.update() # updating the display
smallfont = pygame.font.SysFont("comicsansms", 25) # Define font object
medfont = pygame.font.SysFont("comicsansms", 50) # Define font object
largefont = pygame.font.SysFont("comicsansms", 80) # Define font object
playerSprite = pygame.image.load('sprites/manny.png')
springPlayerSprite = pygame.image.load('sprites/manny spring shoes.png')
speedPlayerSprite = pygame.image.load('sprites/manny speed shoes.png')
speedSprite = pygame.image.load('sprites/speed shoes.png')
jumpSprite = pygame.image.load('sprites/spring shoes.png')
grassBlockSprite = pygame.image.load('sprites/grass.png')
yellowBlockSprite = pygame.image.load('sprites/yellow.png')
blueBlockSprite = pygame.image.load('sprites/blue.png')

#FPS Clock
clock = pygame.time.Clock()

def message_to_screen(msg,color, y_displace=0, size="small"): # Sends a message to screen
    textSurface, textRect = text_objects(msg, color, size)
    textRect.center = (display_width/2), (display_height/2)+y_displace
    gameDisplay.blit(textSurface, textRect)

def text_objects(text, color, size="small"):#Text Sizes
    if size == "small":
        textSurface = smallfont.render(text, True, color)
    elif size == "medium":
        textSurface = medfont.render(text, True, color)
    elif size == "large":
        textSurface = largefont.render(text, True, color)
    return textSurface, textSurface.get_rect()

def button(text, x, y, width, height, inactive_color, active_color, action = None):
    cur = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    if x + width > cur[0] > x and y + height > cur[1] > y:
        pygame.draw.rect(gameDisplay, active_color, (x, y, width, height))
        if click[0] == 1 and action != 1:
            if action == "play":
                gameLoop()
            if action == "info":
                pass
            if action == "quit":
                pygame.quit()
    else:
        pygame.draw.rect(gameDisplay, inactive_color, (x, y, width, height))        

    textSurface, textRect = text_objects(text, black)
    textRect.center = ((x+(width/2)), y+(height/2))
    gameDisplay.blit(textSurface, textRect)

class world():
    def __init__(self):
        self.block_size = 50
        self.mapWidth = 60
        self.mapHeight = 12
        self.blockMap = []
        for row in xrange(self.mapWidth): self.blockMap += [[0]*self.mapHeight] #makes blockmap a 2d array [mapWidth][mapHeight]

        for x in xrange(self.mapWidth):
            for y in xrange(self.mapHeight):
                self.blockMap[x][y] = 0   #fills the map with blank
                if y == 11:
                        self.blockMap[x][y] = 1

        # set up a quick test map
        for i in xrange(5):
            self.blockMap[9+i][8] = 2

        self.blockMap[18][6] = 3

        for i in xrange(5):
            self.blockMap[23+i][6] = 2
        for i in xrange(4):
            self.blockMap[25][7+i] = 3

    def getTileAt(self, x, y):
        tileX = x /self.block_size
        tileY = y /self.block_size

        return self.blockMap[tileX][tileY]

    def render(self, camera):
        #renders map for each tile type
        gameDisplay.fill(white)
        for x in xrange(self.mapWidth):
            for y in xrange(self.mapHeight):
                if self.blockMap[x][y] == 1:
                    gameDisplay.blit(grassBlockSprite, (x*self.block_size - camera,y*self.block_size))
                elif self.blockMap[x][y] == 2:
                    gameDisplay.blit(yellowBlockSprite, (x*self.block_size - camera,y*self.block_size))
                elif self.blockMap[x][y] == 3:
                    gameDisplay.blit(blueBlockSprite, (x*self.block_size - camera,y*self.block_size))

class player():
    def __init__(self):
        self.x = 300
        self.y = 450
        self.width = 38
        self.height = 64
        self.speed = 10
        self.jump_base = self.y
        self.xchange = 0
        self.ychange = 0
        self.is_jumping = False
        self.can_jump = True
        self.jump_height = 100
        self.powered = 0
        self.powered_counter = 0
        self.sprite = playerSprite
    def jump(self):
        self.is_jumping = True
        self.jump_base = self.y
        self.can_jump = False
    def move(self):
        self.x+=self.xchange
        self.y+=self.ychange
        if self.is_jumping == True:
            self.y-=10
            if self.y == self.jump_base - self.jump_height:
                self.is_jumping = False

    def power_up(self):
        if self.powered == 0:
            self.jump_height = 160
            self.speed = 10
        if self.powered == speed:
            self.jump_height = 70
            self.powered_counter+=1
            self.sprite = speedPlayerSprite
        if self.powered == jump:
            self.speed = 15
            self.powered_counter+=1
            self.sprite = springPlayerSprite
        if self.powered_counter == 500:
            self.powered = 0
            self.powered_counter = 0
            self.sprite = playerSprite
    def render(self):
        gameDisplay.blit(self.sprite, (300, self.y))
    def update(self):
        self.move()
        self.power_up()
    
class power_up():
    def __init__(self, x, y, type):
        self.x = x
        self.y = y
        self.width = 10
        self.height = 5
        self.type = type
        if type == speed:
            self.sprite = jumpSprite
        if type == jump:
            self.sprite = jumpSprite
    def render(self, camera):
        gameDisplay.blit(self.sprite, (self.x-camera, self.y))

def collision(object1, object2):
    if object1.x + object1.width > object2.x and object1.x < object2.x + object2.width:
        if object1.y + object1.width > object2.y and object1.y < object2.y + object2.width:
            return True

def game_menu():
    menu = True

    while menu:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    menu = False
                if event.key == pygame.K_q:
                    pygame.quit
                    quit()
        gameDisplay.fill(white)
        message_to_screen("Welcome to the game", green, -100, "large")
        message_to_screen("Complete the level", black, -30)
        message_to_screen("and collect power-ups", black, 10)
        message_to_screen("Fill with content...", black, 50)

        button("Play", 150, 500, 100, 50, green, light_green, action="play")    
        button("Info", 350, 500, 100, 50, blue, light_blue, action="info")
        button("Quit", 550, 500, 100, 50, red, light_red, action="quit")

        pygame.display.update()
        clock.tick(15)
           
def gameLoop():

    gameExit = False;
    cameraX = 0
    World = world()
    Player = player()
    speed_power_up = power_up(902, 284, speed)
  
    #Main Game Loop
    while not gameExit: # Checking if game is active

        
        for event in pygame.event.get(): # Game Exit
            if event.type == pygame.QUIT:
                gameExit = True;
            if event.type == pygame.KEYDOWN: # Keydown event 
                if event.key == pygame.K_LEFT:
                    Player.xchange = -Player.speed
                elif event.key == pygame.K_RIGHT:
                    Player.xchange = Player.speed
                elif event.key == pygame.K_UP and Player.can_jump == True:
                    Player.jump()
        
        
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    Player.xchange = 0
                if event.key == pygame.K_RIGHT:
                    Player.xchange = 0

        if Player.xchange < 0:
            if World.getTileAt(Player.x + Player.xchange, Player.y) != 0:
                Player.xchange = 0
        elif Player.xchange > 0:
            if World.getTileAt(Player.x + Player.width + Player.xchange, Player.y) != 0:
                Player.xchange = 0

        if Player.is_jumping == False:
            if World.getTileAt(Player.x +Player.width/2, Player.y + Player.height + Player.speed) == 0:
                Player.ychange = Player.speed
                Player.can_jump = False
            else:
                Player.can_jump = True
                Player.ychange = 0
        
        if collision(Player, speed_power_up) == True:
            player.speed = 15            
            
        Player.update()
        cameraX = Player.x - 300
       
        World.render(cameraX)
        Player.render()
        speed_power_up.render(cameraX)
        

        pygame.display.update() # Updates the display/must use this 

        clock.tick(fps)# Game FPS
        
    message_to_screen("You Lose", red)
    pygame.display.update()
    pygame.quit()
game_menu()
gameLoop()
