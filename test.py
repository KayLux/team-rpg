import pygame
import time
import random

pygame.init()

#Color Defines
white = (255,255,255)
black = (0,0,0)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)



#Game/Display Setup
gameDisplay = pygame.display.set_mode((800, 600)) #Creating Display surface
pygame.display.set_caption('Team Peter The Game')# Window heading
pygame.display.update() # updating the display
font = pygame.font.SysFont(None, 25) # Define font object
img = pygame.image.load('player.png')


#Game Variables
display_width = 800
display_height = 600
block_size = 10
fps = 15

#FPS Clock
clock = pygame.time.Clock()

def message_to_screen(msg,color): # Sends a message to screen
    screen_text = font.render(msg, True, color) # rendering screen_text var
    gameDisplay.blit(screen_text, [display_width/2, display_height/2])# Draws the text on display surface

def gameLoop():

    gameExit = False;
    lead_x = 300
    lead_y = 300
    lead_x_change = 0
    lead_y_change = 0

    randAppleX = random.randrange(0, display_width-block_size)
    randAppleY = random.randrange(0, display_height-block_size)

    #Main Game Loop
    while not gameExit: # Checking if game is active

        for event in pygame.event.get(): # Game Exit
            if event.type == pygame.QUIT:
                gameExit = True;
            if event.type == pygame.KEYDOWN: # Keydown event 
                if event.key == pygame.K_LEFT:
                    lead_x_change = -10
                    lead_y_change = 0  # Stops diaganal movment
                elif event.key == pygame.K_RIGHT:
                    lead_x_change = 10
                    lead_y_change = 0
                elif event.key == pygame.K_UP:
                    lead_y_change = -10
                    lead_x_change = 0
                elif event.key == pygame.K_DOWN:
                    lead_y_change = 10
                    lead_x_change = 0
            if event.type == pygame.KEYUP: # Key release - stops char moving
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                   lead_x_change = 0
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                   lead_y_change = 0


        if lead_x >= 800 or  lead_x <= 0 or lead_y >= 600 or lead_y <= 0: # Checking the boundries
            gameExit = True
          
                            
        lead_x += lead_x_change # Allows keys to be held down - Adds lead_x_change onto current position
        lead_y += lead_y_change

        
        gameDisplay.fill(white)# White background

        gameDisplay.blit(img, (lead_x, lead_y))
        pygame.display.update() # Updates the display/must use this 

        clock.tick(fps)# Game FPS


        
    message_to_screen("You Lose", red)
    pygame.display.update()
    time.sleep(2)
    pygame.quit()
    quit()
gameLoop()
